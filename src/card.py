import random

class Rank:
    def __init__(self, name: str, weight: int, amount: int):
        self.name = name
        self.weight = weight
        self.amount = amount

    def __lt__(self, other):
        return self.weight < other.weight

    def __repr__(self):
        return self.name

class Typing:
    def __init__(self, initial: str, color: str):
        self.initial = initial
        self.color = color

    def __repr__(self):
        return self.initial


class Card:
    def __init__(self, rank: Rank, typing: Typing):
        self.rank = rank
        self.typing = typing
        self.name = f"{self.rank}{self.typing}"

    def get_typing(self):
        return self.typing.initial
    
    def __lt__(self, other):
        if self.typing == other.typing:
            return self.rank < other.rank
        raise ValueError("Cannot compare cards with different typing.")

    def __repr__(self):
        return f'{self.typing.color}{self.name}\033[00m'


class Deck:
    ACE = Rank("A", 10, 11)
    THREE = Rank("3", 9, 10)
    REI = Rank("R", 8, 4)
    CABAL = Rank("C", 7, 3)
    SOTA = Rank("S", 6, 2)
    SEVEN = Rank("7", 5, 0)
    SIX = Rank("6", 4, 0)
    FIVE = Rank("5", 3, 0)
    FOUR = Rank("4", 2, 0)
    TWO = Rank("2", 1, 0)
    KABOUYA = Typing("K", '\033[95m')
    SIF = Typing("S", '\033[96m')
    COPAS = Typing("C", '\033[91m')
    DHB = Typing("D", '\033[93m')
    RANKS = [ACE, THREE, REI, CABAL, SOTA,
            SEVEN, SIX, FIVE, FOUR, TWO]
    TYPINGS = [KABOUYA, SIF, COPAS, DHB]
    def __init__(self):
        self.cards = []

    def initialize(self):
        for typing in self.TYPINGS:
            for rank in self.RANKS:
                self.cards.append(Card(rank, typing))
        
    def shuffle(self):
        random.shuffle(self.cards)

    def soft_shuffle(self):
        for _ in range(20):
            chunk_size = random.randint(5, 25)
            start = random.randint(0, len(self.cards) - chunk_size)
            self.cards = self.cards[start:start + chunk_size] + self.cards[:start] + self.cards[start + chunk_size:]

    def pick_cards(self, num: int = 5):
        picked, left = self.cards[:num], self.cards[num:]
        self.cards = left
        return picked
    
    def reset(self):
        self.initialize()

    def __repr__(self):
        s = ''
        for i, card in enumerate(self.cards):
            s += str(card)
            if i % 10 == 9:
                if i != len(self.cards) - 1:
                    s += '\n'
            else:
                s += ' '
        return s
            

if __name__ == '__main__':
    deck = Deck()
    print("Initialize:")
    deck.initialize()
    print(deck)
    deck.soft_shuffle()
    print("Shuffle:")
    print(deck)
