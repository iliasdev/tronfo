from typing import List
from card import Card, Deck
from globals import CALLS


class Player:
    _id = 1
    def __init__(self):
        self.id = self._id
        self.hand = []
        self._bid = None
        self.team = None
        Player._id += 1

    @property
    def bid(self):
        return self._bid

    @bid.setter
    def bid(self, value):
        if value not in CALLS:
            raise ValueError(f"\'{value}\' is not a valid bid. Accepted values: {CALLS}.")
        self._bid = value
    
    def get_typing_from_hand(self, typing):
        return [e for e in self.hand if e.get_typing() == typing]
    
    def sort_hand(self):
        sorted_hand = []
        split_hand = [sorted(self.get_typing_from_hand(t.initial), reverse=True) for t in Deck.TYPINGS]
        split_hand = sorted(split_hand, key=len, reverse=True)
        for l in split_hand:
            sorted_hand += l
        self.hand = sorted_hand

    def add_cards_to_hand(self, cards_list: List[Card]):
        self.hand += cards_list
        self.sort_hand()

    def remove_cards_from_hand(self, cards_list: List[Card]):
        for card in cards_list:
            self.hand.remove(card)
        
    def get_hand_repr(self):
        return ' '.join([str(e) for e in self.hand])

    def __repr__(self):
        return f"Player {self.id}"


class Team:
    _id = 1
    def __init__(self):
        self.id = self._id
        self.player1 = None
        self.player2 = None
        self.taken = []
        self.bonuses = 0
        self.score = 0
        Team._id += 1

    def join_players(self, player1: Player, player2: Player):
        self.player1 = player1
        self.player2 = player2
        self.player1.team = self
        self.player2.team = self
    
    def add_cards_to_taken(self, cards_list: List[Card]):
        self.taken += cards_list

    def get_total(self):
        total = 0
        for card in self.taken:
            total += card.rank.amount
        return total

    def __repr__(self):
        return f"Team {self.id}: {self.player1}, {self.player2}"


if __name__ == '__main__':
    from card import Deck
    deck = Deck()
    deck.initialize()
    deck.soft_shuffle()
    player1 = Player()
    player2 = Player()
    player1.add_cards_to_hand(deck.pick_cards())
    player2.add_cards_to_hand(deck.pick_cards())
    player1.add_cards_to_hand(deck.pick_cards())
    player2.add_cards_to_hand(deck.pick_cards())
    print(player1, player1.get_hand_repr())
    print(player2, player2.get_hand_repr())
    team = Team()
    team.join_players(player1, player2)
    print(team)
    team.add_cards_to_taken(player1.hand + player2.hand)
    print(team.get_total())
